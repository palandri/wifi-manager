#include "Configuration.h"
#include <LittleFS.h>

const char ssid_key[] = "ssid";
const char pass_key[] = "pass";
const char hostname_key[] = "hostname";

bool prefsInitialized;

Configuration::Configuration()
{
	// File paths to save input values permanently
	ssidPath = "/ssid.txt";
	passPath = "/pass.txt";
	hostnamePath = "/hostname.txt";

	prefsInitialized = false;
	fsInitialized = false;
}

void Configuration::initFS()
{
	if (fsInitialized)
	{
		return;
	}

	fsInitialized = true;

	if (!LittleFS.begin(true))
	{
		Serial.println("An error has occurred while mounting FS");
		return;
	}

	Serial.println("FS mounted successfully");
}

void Configuration::writeSSID(const char *ssid)
{
	writeFile(LittleFS, ssidPath, ssid);
}

void Configuration::writePass(const char *pass)
{
	writeFile(LittleFS, passPath, pass);
}

void Configuration::writeHostname(const char *hostname)
{
	writeFile(LittleFS, hostnamePath, hostname);
}

String Configuration::getSSID()
{
	return readFile(LittleFS, ssidPath);
}

String Configuration::getPass()
{
	return readFile(LittleFS, passPath);
}

String Configuration::getHostname()
{
	return readFile(LittleFS, hostnamePath);
}

// Read File from FS
String Configuration::readFile(fs::FS &fs, const char *path)
{
	initFS();

	Serial.printf("Reading file: %s\r\n", path);

	File file = fs.open(path);
	if (!file || file.isDirectory())
	{
		Serial.println("- failed to open file for reading");
		return String();
	}

	String fileContent;
	while (file.available())
	{
		fileContent = file.readStringUntil('\n');
		break;
	}

	return fileContent;
}

// Write file to FS
void Configuration::writeFile(fs::FS &fs, const char *path, const char *message)
{
	initFS();

	Serial.printf("Writing file: %s\r\n", path);

	File file = fs.open(path, FILE_WRITE);
	if (!file)
	{
		Serial.println("- failed to open file for writing");
		return;
	}
	if (file.print(message))
	{
		Serial.println("- file written");
	}
	else
	{
		Serial.println("- write failed");
	}
}
